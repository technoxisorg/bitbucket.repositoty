﻿using Repo.Models.Context;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Repo.Models.CustumModels;
using System.Collections;

namespace Repo.Repository
{
    public class ChatRepository : IChat
    {
        private SampleDbContext _context;

        public ChatRepository(SampleDbContext context)
        {
            _context = context;
        }
        public List<UserMessages> getMessages(string userId,string senderId)
        {
            List<UserMessages> s = new List<UserMessages>();
            s = _context.userMessages.Where(x=>x.groupName== userId+"/"+senderId || x.groupName== senderId+"/"+userId).ToList();
            _context.userMessages.Where(x => x.groupName == userId + "/" + senderId || x.groupName == senderId + "/" + userId).ToList().ForEach(a => a.haveSeenReceiver = true);

            _context.SaveChanges();
            return s;
        }
        public string haveSeen(string userId, string senderId)
        {
            List<UserMessages> s = new List<UserMessages>();
            _context.userMessages.Where(x => x.groupName == userId + "/" + senderId || x.groupName == senderId + "/" + userId).ToList().ForEach(a => a.haveSeen = true);

            _context.SaveChanges();
            return "";
        }
       
        public IList getCount(IList users, string userId)
        {
            List<UserMessages> s = new List<UserMessages>();
            //IList d;
            for (int i = 0; i < users.Count; i++)
            {
               s  = _context.userMessages.Where(x => x.groupName == userId + "/" + users[i] || x.groupName == users[i] + "/" + userId).ToList();

            }
            return s;
        }
        public bool saveMessages(UserMessages request)
        {
            UserMessages s = new UserMessages();
            try
            {
                s.Date = DateTime.Now;
                s.groupName = request.msgFrom + "/" + request.msgTo;
                s.haveSeen = false;
                s.haveSeenReceiver = false;
                s.message = request.message;
                s.msgFrom = request.msgFrom;
                s.msgTo = request.msgTo;
                s.fileName = request.fileName;
                s.fileType = request.fileType;
                s.filePath = request.filePath;
                _context.userMessages.Add(s);
                _context.SaveChanges();
                var Response = true;
                return Response;
            }
            catch (Exception e)
            {
                return false;
            }

        }
    }
}
