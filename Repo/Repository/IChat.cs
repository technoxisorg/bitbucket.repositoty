﻿
using Repo.Models.Context;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Repo.Repository
{
    public interface IChat
    {
        List<UserMessages> getMessages(string userId, string senderId);
        string haveSeen(string userId, string senderId);
        IList getCount(IList users, string userId);
        bool saveMessages(UserMessages request);
    }
}
