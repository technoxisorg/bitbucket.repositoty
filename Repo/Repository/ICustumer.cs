﻿using Repo.Models.Context;
using Repo.Models.CustumModels;
//using Repo.Models.CustumModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repo.Repository
{
   public interface ICustumer
    {
        CumCustumer getCustumersRepo(searchpage request);
        CumCustumer SortCustumerDetails(string data,string sort);
        Custumer getCustumerDetailsById(int Id);
        string saveCustumerDetails(Custumer request);
        string updateCustumerDetails(Custumer request);
        string deleteCustumerDetailsById(int Id);
        List<Custumer> ExportData();
        string GetHTMLString();
    }
}
