﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Repo.Migrations
{
    public partial class UserMessages : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "haveSeenReceiver",
                table: "userChat",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "haveSeenReceiver",
                table: "userChat");
        }
    }
}
