﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Repo.Models
{
    public class Response
    {
        public string data { get; set; }
        public string token { get; set; }
        public string userId { get; set; }
        public bool res { get; set; }
        //public string Email { get; set; }
        //public string PhoneNumber { get; set; }
    }
}
