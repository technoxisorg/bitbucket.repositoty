﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repo.Models.Context
{
    public class SampleDbContext : DbContext
    {
        public SampleDbContext(DbContextOptions<SampleDbContext> options) : base(options)
        {
        }
       
        public DbSet<SampleModel> samDetails { get; set; }
        public DbSet<Custumer> custumerDetails { get; set; }
        public DbSet<UserMessages> userMessages { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SampleModel>().ToTable("SampleModel");
            modelBuilder.Entity<Custumer>().ToTable("Custumer");
            modelBuilder.Entity<UserMessages>().ToTable("userChat");
        }
    }
}
